from django import forms

from .models import Users

class AddUserForm(forms.ModelForm):
    class Meta:
        model = Users
        fields = ('user_name', 'user_email',)

    
    def clean(self):
        cleaned_data = super().clean()
        data_email = cleaned_data.get('user_email')
        data_name = cleaned_data.get('user_name')

        if not data_email:
            raise forms.ValidationError("You have filled the email!")
        if not data_name:
            raise forms.ValidationError("You have filled the name!")