from tastypie.resources import ModelResource
from users.models import Users
from tastypie.authorization import Authorization
class UsersResource(ModelResource):
    class Meta:
        queryset = Users.objects.all()
        resource_name = 'users'
        authorization = Authorization()