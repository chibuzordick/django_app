from django.shortcuts import render

# Create your views here.

from users.models import Users
from .forms import AddUserForm

from users.resources import UsersResource
users_resource = UsersResource()

def index(request):
    """View function for home page of site."""
    # Generate counts of some of the main objects
    num_users = Users.objects.count()
   
    context = {
        'num_users': num_users,
    }
    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)

def listUsers(request):
    num_users = Users.objects.count()
    users_list = []
    
    users_list += Users.objects.all()
    context = {
        'num_users': num_users,
        'users_list': users_list
    }
    return render(request, 'list.html', context=context)

def addUsers(request):
    if request.method == 'GET':
        form = AddUserForm()
        return render(request, 'add.html', {'form': form})
    user = Users(user_name=request.POST.get('user_name', ''), user_email=request.POST.get('user_email', ''))
    user.save()
    return render(request, 'index.html')
    
    


