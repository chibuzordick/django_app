from django.db import models

# Create your models here.

class Users(models.Model):
   
    user_name = models.CharField(max_length= 255, blank=False)
    user_email = models.CharField(max_length= 255, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user_name+ " "+ self.user_email